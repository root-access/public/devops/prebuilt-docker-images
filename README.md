# prebuild-docker-images
This repository will build docker images with pre-built dependencies/libraries and push them to Docker Hub.

## [`rootaccess/php`][rootaccess/php]
* `rootaccess/php:5.6-cli`
* `rootaccess/php:5.6-fpm`
* `rootaccess/php:7.1-cli`
* `rootaccess/php:7.1-fpm`
* `rootaccess/php:7.2-cli`
* `rootaccess/php:7.2-fpm`
* `rootaccess/php:7.3-cli`
* `rootaccess/php:7.3-fpm`
* `rootaccess/php:7.4-cli`
* `rootaccess/php:7.4-fpm`

## [`rootaccess/node`][rootaccess/node]
* `rootaccess/node:4`, with `node-sass@3.4.2`
* `rootaccess/node:12`, with `dart-sass` and `node-sass@4.12`
* `rootaccess/node:14`, with `dart-sass` and `node-sass@5`


---
[rootaccess/php]: https://hub.docker.com/r/rootaccess/php
[rootaccess/node]: https://hub.docker.com/r/rootaccess/node
